FuryEat is a fictional fast-food written in Assembly MIPS R2000/R3000 using SPIM S20 emulator.

With this academic project, you can choose, order and pay all the food in this restaurant.

This is my project for Architettura degli Elaboratori 2 exam in the University of Milan, Computer Science degree.
