main:

.data
string1: .asciiz "Benvenuto al FuryEat "
string2: .asciiz "Questo è il nostro menu: (Prema 10 quando ha concluso)"
string3: .asciiz "1. Hamburger € 4.00 "
string4: .asciiz "2. Hot Dog € 3.00 "
string5: .asciiz "3. Patatine Fritte € 2.00 "
string6: .asciiz "4. FuryKingXXL € 6.00 "
string7: .asciiz "5. Menu Classic Fury € 7.00 "
string8: .asciiz "6. Fury Nuggets € 3.00 "
string9: .asciiz "7. Acqua naturale o frizzante(33cl) € 1.00 "
string10: .asciiz "8. CocaCola(33cl) € 2.00 "
string11: .asciiz "9. Club Sandwich € 6.00 "
string13: .asciiz "10. Ordine concluso"
string12: .asciiz "Inserisca il numero del prodotto che vuole ordinare "
string_a_capo: .asciiz "\n"				
string14: .asciiz "Inserire un numero da 1 a 10		"
string16: .asciiz "Quantita : (Inserisca numero negativo per tornare al menu di partenza azzerando l'intera transizione) "
string17: .asciiz "Per cancellare un prodotto inserito è necessario scegliere il numero del prodotto da cancellare e inserire\ncome quantita 0. Qualsiasi lettera o parole sarà interpretata come il pulsante 0."
string18: .asciiz "Se desidera ordinare altro o modificare l'ordine inserisca 0, altrimenti qualsiasi altro numero: "
string19: .asciiz "Scontrino: "
string20: .asciiz "x"
string21: .asciiz "€ "
string22: .asciiz "\t\t\t Totale prodotto: "
string23: .asciiz "\t Totale prodotto: "
string24: .asciiz "\t\t\t\t Totale prodotto: "
string25: .asciiz "Se ordina lo stesso prodotto due volte le verra' riportato esclusivamente l'ultimo cronologicamente inserito."
string26: .asciiz "\t\t\t\t\tTotale scontrino: "
string27: .asciiz "--------------------------------------------------------------------------------------------- "
string28: .asciiz "Metodo di pagamento? Inserisca: \n1. Contanti \n2. Carta di Credito \n3. Annulla l'intera transizione e uscita dal programma\n4. Azzeramento della transizione e ritorno al menu iniziale"
string29: .asciiz "Importo effettuato con successo. Ritiri il prodotto in cassa, grazie per avere scelto il FuryEat.\nGrazie e Arrivederci "
string30: .asciiz "Inserisca il valore dell'importo che vuole effettuare (Inserisca 0 per tornare ai metodi di pagamento) : € "
string31: .asciiz "Resto : "
string32: .asciiz "Annullamento effettuato con successo. Grazie e arrivederci"
string33: .asciiz "Inserire una cifra tra 1 e 4\n"
string34: .asciiz "Importo inserito non copre il prezzo finale!!! "
string35: .asciiz "Nessun prodotto selezionato, Grazie e arrivederci"

.align 2						# Allineo i dati sennò mi da errore nell array
A: .space 36					# Array di 9 elementi,ciascuno di 4 byte(9 sarebbero i panini)

.align 2						# Allineo i dati sennò mi da errore nell array
B: .space 36					# Array quantita 9 elementi


.text

addi $sp, $sp, -4	# faccio spazio di 4 byte dentro lo stack 
sw $ra, 0($sp)		# salvo nello stack l attuale contenuto di $ra(push)

Inizio:
	la $s0, A		# Scrive il base address di A in $s0 (array dei prezzi)
	la $s5, B		# Scrive il base address di B in $s5 (array delle quantita)

############# Userò spesso 
	addi $s4,$zero,11		# $s4 = 11
	addi $s3,$zero,1		# $s3 = 1

####### AZZERAMENTO DELL ARRAY PER RICOMINCIARE SE VOGLIAMO UN ALTRA
####### TRANSIZIONE PREMENDO 3 NEL MENU DEL METODO DI PAGAMENTO

	sw $zero,0($s0)		# azzeramento della prima posizione dell array
	sw $zero,4($s0)		# azzeramento della seconda posizione dell array
	sw $zero,8($s0)		# azzeramento della terza posizione dell array
	sw $zero,12($s0)	# azzeramento della quarta posizione dell array
	sw $zero,16($s0)	# azzeramento della quinta posizione dell array
	sw $zero,20($s0)	# azzeramento della sesta posizione dell array
	sw $zero,24($s0)	# azzeramento della settima posizione dell array
	sw $zero,28($s0)	# azzeramento della ottavo posizione dell array
	sw $zero,32($s0)	# azzeramento della nona posizione dell array



##########    Messaggio di benvenuto

li $v0,4			# selezione di print_string
la $a0,string1		# $a0 = indirizzo di string1
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo
jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string2		# $a0 = indirizzo di string2
syscall				# lancio print_string 

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string25		# $a0 = indirizzo di string2
syscall				# lancio print_string 

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string17		# $a0 = indirizzo di string2
syscall				# lancio print_string 

jal p1				# salto alla procedura per il carattere a capo
jal p1				# salto alla procedura per il carattere a capo

############################STAMPA MENU

li $v0,4			# selezione di print_string
la $a0,string3		# $a0 = indirizzo di string3
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string4		# $a0 = indirizzo di string4
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string5		# $a0 = indirizzo di string5
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string6		# $a0 = indirizzo di string6
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string7		# $a0 = indirizzo di string7
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string8		# $a0 = indirizzo di string8
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string9		# $a0 = indirizzo di string9
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string10		# $a0 = indirizzo di string10
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string11		# $a0 = indirizzo di string11
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo

li $v0,4			# selezione di print_string
la $a0,string13		# $a0 = indirizzo di string13
syscall				# lancio print_string

jal p1				# salto alla procedura per il carattere a capo
jal p1				# salto alla procedura per il carattere a capo



##################FINE MENU

######Se inserisco maggiore di 10 o minore di 1 ri-eseguo
Reinsert:

	li $v0,4			# selezione di print_string
	la $a0,string12		# $a0 = indirizzo di string12
	syscall				# lancio print_string

	jal p2				# salto alla procedura p2(verifica <10 && >0)

	addi $s2,$zero,10	# s2 = 10
	bne $t1, $t2, else1	# compreso tra 1 e 10
	move $s1,$v0		# salva quello che ho scritto in $s1
	beq $s1,$s2,Fine	# se s1 == 10 vado alla label Fine
	j exit1				# salta all label di uscita del ciclo

else1:					# inizio dell else

	jal errato			# salto alla procedura errato per mostrare il messaggio d errore
	j Reinsert			# salta alla label Reinsert(reinserimento dell ordinazione)

exit1:					# fine del ciclo


#controllo che $s1 sia ogni volta uguale al corrispondente numero dell'elenco
#se è uguale allora inserisco nella rispettiva posizione dell'array il prezzo
#per un singolo prodotto e poi esco dall'inserimento

#LI RESETTO ANCHE SUL PRIMO CHE NEL CASO L'UTENTE SCELGA ANCORA 1
#IL SUO VALORE NELL ARRAY SI RESETTA SENNO VENIVA MOLTIPLICATO ANCORA
#PER L ULTIMA QUANTITA INSERITA PER QUEL ELEMENTO

primo:					# Inserimento primo elemento dell array
	addi $t6,$zero,4	# $t6 = 4
	addi $t3,$zero,1	# $t3 = 1
	bne $s1,$t3,secondo # $s1 != 1 controlla se è == 2(va a secondo)
	sw $t6, 0($s0)		# A[0] = 4
	j exitnumeri		# uscita inserimento

secondo:				# Inserimento secondo elemento dell array
	addi $t6,$zero,3	# $t6 = 3
	addi $t3,$zero,2	# $t3 = 2
	bne $s1,$t3,terzo   # s1 != 2 controlla se è == 3 (va a terzo)
	sw $t6, 4($s0)		# A[1] = 6
	j exitnumeri		# uscita inserimento

terzo:					# Inserimento terzo elemento dell array
	addi $t6,$zero,2	# $t6 = 2
	addi $t3,$zero,3	# $t3 = 3
	bne $s1,$t3,quarto  # s1 != 3 controlla se è == 4 (va a quarto)
	sw $t6, 8($s0)		# A[2] = 2
	j exitnumeri		# uscita inserimento

quarto:					# Inserimento quarto elemento dell array
	addi $t6,$zero,6	# $t6 = 6
	addi $t3,$zero,4	# $t3 = 4
	bne $s1,$t3,quinto  # s1 != 4 controlla se è == 5 (va a quinto)
	sw $t6, 12($s0)		# A[3] = 6
	j exitnumeri		# uscita inserimento

quinto:					# Inserimento quinto elemento dell array
	addi $t6,$zero,7	# $t6 = 7
	addi $t3,$zero,5	# $t3 = 5
	bne $s1,$t3,sesto   # s1 != 5 controlla se è == 6 (va a sesto)
	sw $t6, 16($s0)		# A[4] = 7
	j exitnumeri		# uscita inserimento

sesto:					# Inserimento sesto elemento dell array
	addi $t6,$zero,3	# $t6 = 3
	addi $t3,$zero,6	# $t3 = 6
	bne $s1,$t3,settimo # s1 != 6 controlla se è == 7 (va a settimo)
	sw $t6, 20($s0)		# A[5] = 3
	j exitnumeri		# uscita inserimento

settimo:				# Inserimento settimo elemento dell array
	addi $t6,$zero,1	# $t6 = 1
	addi $t3,$zero,7	# $t3 = 7
	bne $s1,$t3,ottavo  # s1 != 7 controlla se è == 8 (va a ottavo)
	sw $t6, 24($s0)		# A[6] = 1
	j exitnumeri		# uscita inserimento

ottavo:					# Inserimento ottavo elemento dell array
	addi $t6,$zero,2	# $t6 = 2
	addi $t3,$zero,8	# $t3 = 8
	bne $s1,$t3,nono    # s1 != 8 controlla se è == 9 (va a nono)
	sw $t6, 28($s0)		# A[7] = 2
	j exitnumeri		# uscita inserimento

nono:					# Inserimento nono elemento dell array
	addi $t6,$zero,6	# $t6 = 6
	addi $t3,$zero,9	# $t3 = 9
	bne $s1,$t3,decimo  # s1 != 9 controlla se è == 10 (va a decimo)
	sw $t6, 32($s0)		# A[8] = 6
	j exitnumeri		# uscita inserimento

decimo:					# Uscita dal menu
	j Fine				# Salta alla procedura di fine di inserimento

exitnumeri:				# Fine inserimento numeri


################# SELEZIONE QUANTITA


jal p1					# salto alla procedura per il carattere a capo

SelezQuantita:			# Seleziona la quantita
	li $v0,4			# selezione di print_string
	la $a0,string16		# $a0 = indirizzo di string13
	syscall				# lancio print_string

#se è = 0 allora azzero il prodotto selezionato, se è minore di 0 azzero tutto.
#se è un altro numero allora moltiplico quel numero per il valore nell'array s0.
#in $t6 ho il prezzo per un singolo prodotto preso precedentemente.
#in $s1 ho il numero che del prodotto che ho selezionato dal menu.

jal leggo_int				# salto alla procedura di lettura dell intero

move $t5,$v0				# sposto in t5 la quantita scritta
slt $t3,$t5,$zero			# se la quantita è < 0 t3 = 1
beq $t3,$s3,Inizio			# se $t3 == 1 allora errato.
mul $t6, $t6,$t5			# $t6 = $t6*$t5 

quant1:						# quantita 1 prodotto
	addi $t1,$zero,1		# $t1 = 1
	bne $s1,$t1,quant2		# != 1 va a quant2
	sw $t6, 0($s0)			# A[0] = totale euro
	sw $t5, 0($s5)			# B[0] = quantita di A[0]
	j exitqnt				# salto alla label di uscita della quantita


quant2:						# quantita 2 prodotto
	addi $t1,$zero,2		# $t1 = 2
	bne $s1,$t1,quant3		# != 2 va a quant3
	sw $t6, 4($s0)			# A[1] = totale euro
	sw $t5, 4($s5)			# B[1] = quantita di A[1]
	j exitqnt				# salto alla label di uscita della quantita

quant3:						# quantita 3 prodotto
	addi $t1,$zero,3		# $t1 = 3
	bne $s1,$t1,quant4		# != 3 va a quant4
	sw $t6, 8($s0)			# A[2] = totale euro
	sw $t5, 8($s5)			# B[2] = quantita di A[2]
	j exitqnt				# salto alla label di uscita della quantita

quant4:						# quantita 4
	addi $t1,$zero,4		# $t1 = 4
	bne $s1,$t1,quant5		# != 4 va a quant5
	sw $t6, 12($s0)			# A[3] = totale euro
	sw $t5, 12($s5)			# B[3] = quantita di A[3]
	j exitqnt				# salto alla label di uscita della quantita

quant5:						# quantita 5
	addi $t1,$zero,5		# $t1 = 5
	bne $s1,$t1,quant6		# != 5 va a quant6
	sw $t6, 16($s0)			# A[4] = totale euro
	sw $t5, 16($s5)			# B[4] = quantita di A[4]
	j exitqnt				# salto alla label di uscita della quantita

quant6:						# quantita 6
	addi $t1,$zero,6		# $t1 = 6
	bne $s1,$t1,quant7		# != 6 va a quant7
	sw $t6, 20($s0)			# A[5] = totale euro
	sw $t5, 20($s5)			# B[5] = quantita di A[5]
	j exitqnt				# salto alla label di uscita della quantita

quant7:						# quantita 7
	addi $t1,$zero,7		# $t1 = 7
	bne $s1,$t1,quant8		# != 7 va a quant8
	sw $t6, 24($s0)			# A[6] = totale euro
	sw $t5, 24($s5)			# B[6] = quantita di A[6]
	j exitqnt				# salto alla label di uscita della quantita

quant8:						# quantita 8
	addi $t1,$zero,8		# $t1 = 8
	bne $s1,$t1,quant9		# != 8 va a quant8
	sw $t6, 28($s0)			# A[7] = totale euro
	sw $t5, 28($s5)			# B[7] = quantita di A[7]
	j exitqnt				# salto alla label di uscita della quantita

quant9:						# quantita 9
	addi $t1,$zero,9		# $t1 = 9
	bne $s1,$t1,exitqnt		# != 9 va a quant8
	sw $t6, 32($s0)			# A[8] = totale euro
	sw $t5, 32($s5)			# B[8] = quantita di A[8]
	j exitqnt				# salto alla label di uscita della quantita

################# FINITO TUTTE LE QUANTITA


exitqnt:					# Label di uscita della quantita
	jal p1					# procedura a capo
	li $v0, 4				# seleziona print_string
	la $a0, string18		# $a0 contiene il ritorno a capo		
	syscall					# lancia print_string

jal leggo_int				# salto alla procedura che mi permette di leggere un int

beq $v0, $zero, Reinsert 	# se inserisco 0 vado alla Label Reinsert


Fine:						# Label di Fine, Stampa lo scontrino

	addi $a0,$zero,9		# $a0 = posizioni dell array
	addi $a1,$s0,0			# $a1 = elementi dell array
	
	jal sommaricorsiva		# salto alla procedura sommaricorsiva
	
	move $t4,$v0			# sposto $v0 in $t4
	beq $t4,$zero,salutiSenzaProd	# if($t4 == 0) then salutiSenzaProd (se è 0 salutiSenzaProd, cioè se il prezzo totale è 0 va alla label salutiSenzaProd, evitando la stampa dello scontrino)


jal p1						# salto alla procedura per il carattere a capo

li $v0, 4					# seleziona print_string
la $a0, string19			# $a0 contiene il ritorno a capo		
syscall						# lancia print_string

jal p1						# salto alla procedura per il carattere a capo

############# STAMPA SCONTRINO SOLO != DA 0
		
	
#Primo Elemento

lw $t6, 0($s0)			# $t6 = 0($s0) metto in $t6 il primo elemento dell array dei prezzi
lw $t5, 0($s5)			# $t5 = 0($s5) metto in $t5 il primo elemento dell array delle quantita
beq $t6,$zero,vai2		# if($t6 == 0) then vai2 (se è 0 vai2, cioè se il prezzo è 0 va alla label vai2, evitando la stampa)
li $v0,4				# selezione di print_string
la $a0,string3			# $a0 = indirizzo di string3
syscall					# lancio print_string
jal stampax				# salta alla procedura di stampa della "X"
jal stampaqnt			# salta alla procedura di stampa della quantita
li $v0,4				# selezione di print_string
la $a0,string24			# $a0 = indirizzo di string13
syscall					# lancio print_string
jal stampaeuro			# salta alla proceudra di stampa di "€"
jal stampa				# salta alla proceudra della stampa del prezzo finale per il prodotto
jal p1					# salto alla procedura per il carattere a capo

#Secondo Elemento

vai2:					# stampa secondo prodotto
	lw $t6, 4($s0)		# $t6 = 4($s0) metto in $t6 il secondo elemento dell array dei prezzi
	lw $t5, 4($s5)		# $t5 = 4($s5) metto in $t5 il secondo elemento dell array delle quantita	
	beq $t6,$zero,vai3	# if($t6 == 0) then vai3 (se è 0 vai3, cioè se il prezzo è 0 va alla label vai3, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string4		# $a0 = indirizzo di string4
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	li $v0,4			# selezione di print_string
	la $a0,string24		# $a0 = indirizzo di string13
	syscall				# lancio print_string
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Terzo Elemento

vai3:					# stampa terzo prodotto
	lw $t6, 8($s0)		# $t6 = 8($s0) metto in $t6 il terzo elemento dell array dei prezzi
	lw $t5, 8($s5)		# $t5 = 8($s5) metto in $t5 il terzo elemento dell array delle quantita
	beq $t6,$zero,vai4	# if($t6 == 0) then vai4 (se è 0 vai4, cioè se il prezzo è 0 va alla label vai4, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string5		# $a0 = indirizzo di string5
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	jal stampatab		# salta alla procedura di stampa dello spazio(per allineare)
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Quarto Elemento

vai4:					# stampa quarto prodotto
	lw $t6, 12($s0)		# $t6 = 12($s0) metto in $t6 il quarto elemento dell array dei prezzi
	lw $t5,	12($s5)		# $t5 = 12($s5) metto in $t5 il quarto elemento dell array delle quantita
	beq $t6,$zero,vai5	# if($t6 == 0) then vai5 (se è 0 vai5, cioè se il prezzo è 0 va alla label vai5, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string6		# $a0 = indirizzo di string6
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	jal stampatab		# salta alla procedura di stampa dello spazio(per allineare)
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Quinto Elemento

vai5:					# stampa quinto elemento
	lw $t6, 16($s0)		# $t6 = 16($s0) metto in $t6 il quinto elemento dell array dei prezzi	
	lw $t5, 16($s5)		# $t6 = 16($s5) metto in $t5 il quinto elemento dell array delle quantita
	beq $t6,$zero,vai6	# if($t6 == 0) then vai6 (se è 0 vai6, cioè se il prezzo è 0 va alla label vai6, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string7		# $a0 = indirizzo di string7
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	jal stampatab		# salta alla procedura di stampa dello spazio(per allineare)
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Sesto Elemento

vai6:					# stampa sesto elemento
	lw $t6, 20($s0)		# $t6 = 20($s0) metto in $t6 il sesto elemento dell array dei prezzi
	lw $t5, 20($s5)		# $t5 = 20($s5) metto in $t5 il sesto elemento dell array delle quantita 
	beq $t6,$zero,vai7	# if($t6 == 0) then vai7 (se è 0 vai7, cioè se il prezzo è 0 va alla label vai7, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string8		# $a0 = indirizzo di string8
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	jal stampatab		# salta alla procedura di stampa dello spazio(per allineare)
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Settimo Elemento

vai7:					# Stampa settimo elemento
	lw $t6, 24($s0)		# $t6 = 24($s0) metto in $t6 il settimo elemento dell array dei prezzi
	lw $t5, 24($s5)		# $t5 = 24($s5) metto in $t5 il settimo elemento dell array delle quantita 
	beq $t6,$zero,vai8	# if($t6 == 0) then vai8 (se è 0 vai8, cioè se il prezzo è 0 va alla label vai8, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string9		# $a0 = indirizzo di string9
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	li $v0,4			# selezione di print_string
	la $a0,string23		# $a0 = indirizzo di string13
	syscall				# lancio print_string
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Ottavo Elemento

vai8:					# Stampa ottavo elemento
	lw $t6, 28($s0)		# $t6 = 28($s0) metto in $t6 l ottavo elemento dell array dei prezzi
	lw $t5, 28($s5)		# $t5 = 28($s5) metto in $t5 l ottavo elemento dell array delle quantita 	
	beq $t6,$zero,vai9	# if($t6 == 0) then vai9 (se è 0 vai9, cioè se il prezzo è 0 va alla label vai9, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string10		# $a0 = indirizzo di string10
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	jal stampatab		# salta alla procedura di stampa dello spazio(per allineare)
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto
	jal p1				# salto alla procedura per il carattere a capo

#Nono Elemento

vai9:					# Stampa nono elemento
	lw $t6, 32($s0)		# $t6 = 32($s0) metto in $t6 il nono elemento dell array dei prezzi
	lw $t5, 32($s5)		# $t5 = 32($s5) metto in $t5 il nono elemento dell array delle quantita 	
	beq $t6,$zero,exitstamp 	# if($t6 == 0) then exitstamp (se è 0 exitstamp, cioè se il prezzo è 0 va alla label exitstamp, evitando la stampa)
	li $v0,4			# selezione di print_string
	la $a0,string11		# $a0 = indirizzo di string11
	syscall				# lancio print_string
	jal stampax			# salta alla procedura di stampa della "X"
	jal stampaqnt		# salta alla procedura di stampa della quantita
	jal stampatab		# salta alla procedura di stampa dello spazio(per allineare)
	jal stampaeuro		# salta alla proceudra di stampa di "€"
	jal stampa			# salta alla proceudra della stampa del prezzo finale per il prodotto


exitstamp:				# Fine stampa scontrino

	jal p1				# salto alla procedura per il carattere a capo

	li $v0,4			# selezione di print_string
	la $a0,string27		# $a0 = indirizzo di string27
	syscall				# lancio print_string 

	jal p1				# salto alla procedura per il carattere a capo

	li $v0,4			# selezione di print_string
	la $a0,string26		# $a0 = indirizzo di string26
	syscall				# lancio print_string 

	jal stampaeuro		# salto alla procedura stampaeuro

	li $v0,1			# selezione di print_int
	move $a0,$t4		# $a0 = indirizzo di $t4
	syscall				# lancio print_int


########################################### 

jal p1						# salto alla procedura per il carattere a capo
jal p1						# salto alla procedura per il carattere a capo

Pagamento:					#scelta Metodo di pagamento

	li $v0,4				# selezione di print_string
	la $a0,string28			# $a0 = indirizzo di string1
	syscall					# lancio print_string 

	jal p1					# salto alla procedura per il carattere a capo

	jal leggo_int			# salto alla procedura per leggere un intero

	move $t1,$v0			# in $t1 ho la scelta
	addi $t2,$zero,2		# $t2 = 2
	addi $t3,$zero,3		# $t3 = 3
	addi $t5, $zero,4		# $t5 = 4
	beq $t1,$s3,Contanti	# se $t1 == 1 vado in Contanti
	beq $t1,$t2,CartadiCred	# se $t1 == 2 vado a CartadiCred
	beq $t1,$t3,Annulla		# se $t1 == 3 vado a Annulla
	beq $t1,$t5,Ritorno		# se $t1 == 4 vado a Ritorno

####   MESSAGGIO DI ERRORE

li $v0, 4					# seleziona print_string
la $a0, string33    		# $a0 contiene stringa errore		
syscall						# lancia print_string

j Pagamento					# se inserisco != 0,1,2 o 3 salto a Pagamento(torno indietro)

Contanti:					# Label Contanti 
	
	li $v0, 4				# seleziona print_string
	la $a0, string30	    # $a0 contiene string30		
	syscall					# lancia print_string

	jal leggo_int			# salto alla procedura leggo_int

	move $t6,$v0			# $t6 = importo 
	beq $t6,$zero,Pagamento	# se $t6 == 0 torna a Pagamento
	sub $t7,$t6,$t4			# $t7 = $t6-$t4 (resto)
	addi $t3,$zero,0		# $t3 = 0 
	slt $t3,$t7,$zero		# se $t7 < 0 allora $t3 = 1
	beq $t3,$s3,CreditoIns  # se $t3 == 1 salto a CreditoIns

	li $v0, 4				# seleziona print_string
	la $a0, string31    	# $a0 contiene string31		
	syscall					# lancia print_string

	jal stampaeuro			# salta alla proceudra di stampa di "€"

	li $v0, 1				# seleziona print_int
	add $a0,$zero,$t7		# $a0 contiene resto		
	syscall					# lancia print_int
	
	jal p1					# salto alla procedura per il carattere a capo
	
	li $v0, 4				# seleziona print_string
	la $a0, string29	   	# $a0 contiene string29	
	syscall					# lancia print_string

	j Arrivederci			# salto a Arrivederci

CreditoIns:					# Label Credito Insufficiente
	li $v0, 4				# seleziona print_string
	la $a0, string34    	# $a0 contiene string34	
	syscall					# lancia print_string
	j Contanti				# salto a Contanti
	

CartadiCred:				# Label CartadiCred
	li $v0, 4				# seleziona print_string
	la $a0, string29	   	# $a0 contiene stringa saluto
	syscall					# lancia print_string
	j Arrivederci			# salto a Arrivederci
		
Annulla:					# Label Annulla
	li $v0, 4				# seleziona print_string
	la $a0, string32	   	# $a0 contiene stringa annullamento	
	syscall					# lancia print_string
	j Arrivederci			# salto a Arrivederci

Ritorno:					# Ritorno all Inserimento
	j Inizio				# salto a Reinsert

Arrivederci:				# Label Conclusione
	lw $ra, 0($sp)			# leggo dallo stack
	addi $sp,$sp,4			# aggiorno lo stack pointer(pop)

	jr $ra

			#####Procedure

p1:								#Procedura per il ritorno a capo 
	li $v0, 4					# seleziona print_string
	la $a0, string_a_capo		# $a0 contiene il ritorno a capo		
	syscall						# lancia print_string
	jr $ra						# ritorno alla procedura precedente

p2:								#Procedura numero valido < 11  && >0
	li $v0, 5					# seleziona read_int
	syscall						# lancia read_int
	slt $t1, $v0, $s4			# assegna 1 in $t1 se scrivo <11
	slt $t2, $zero, $v0			# assegna 1 in $t2 se scrivo >0
	jr $ra						# ritorno alla procedura precedente

salutiSenzaProd:				# Uscita
	li $v0, 4					# seleziona print_string
	la $a0, string35  			# $a0 contiene stringa errore		
	syscall						# lancia print_string
	j Arrivederci				# stampa alla procedura Arrivederci
	jr $ra						# ritorno alla procedura precedente


errato:							# Dato errato
	li $v0, 4					# seleziona print_string
	la $a0, string14 		 	# $a0 contiene stringa errore		
	syscall						# lancia print_string
	jr $ra						# ritorno alla procedura precedente

leggo_int: 						# Lettura intero
	li $v0, 5					# seleziona read_int
	syscall						# lancia read_int
	jr $ra						# ritorno alla procedura precedente

stampa:							#STAMPA IL PREZZO FINALE
	li $v0,1					# seleziona print_int
	move $a0, $t6				# $a0 contiene prezzo finale
	syscall						# lancia print_int
	jr $ra						# ritorno alla procedura precedente

stampax:						# STAMPA IL "X"
	li $v0,4					# seleziona print_string
	la $a0,string20				# $a0 contiene string20
	syscall						# lancia print_int
	jr $ra						# ritorno alla procedura precedente

stampaqnt:						# stampa la quantita
	li $v0,1					# seleziona print_int
	move $a0,$t5				# $a0 contiene quantita finale
	syscall						# lancia print_int
	jr $ra						# ritorno alla procedura precedente

stampaeuro:						# STAMPA "€"
	li $v0,4					# seleziona print_string
	la $a0,string21				# $a0 contiene string21
	syscall						# lancia print_int
	jr $ra						# ritorno alla procedura precedente

stampatab:						# STAMPA IL tab
	li $v0,4					# seleziona print_string
	la $a0,string22				# $a0 contiene string22
	syscall						# lancia print_int
	jr $ra						# ritorno alla procedura precedente

sommaricorsiva:							
	addi $sp, $sp, -8 			# creo uno spazio di 8 byte per 2 elementi nello stack
	addi $t0, $a0, -1			# salvo in $t0, $a0-1
	sw $t0, 0($sp)				# Salvo sullo dim-1
	sw $ra, 4($sp)				# Salvo sullo stack $ra e dim-1
	bne $a0, $zero, ricorsione 	# salto a ricorsione se la dimensione dell'array non e' zero
	li $v0, 0 					# Caso base: se array vuoto ritorna 0
	addi $sp, $sp, 8 			# dealloco stack frame
	jr $ra						# ritorno alla procedura chiamante

ricorsione:
	move $a0, $t0 				# aggiorno secondo argomento
	jal sommaricorsiva			# chiamata ricorsiva

	# (ora in $v0 ho f4(arr, dim-1))

	lw $t0, 0($sp)				# ripristino dim-1
	mul $t1, $t0, 4 			# lo moltiplico per 4 e lo metto in $t1
	add $t1, $t1, $a1 			# indirizzo di arr[dim-1]
	lw $t2, 0($t1) 				# t2 = arr[dim-1]
	add $v0, $v0, $t2			# $v0 = + f4(arr, dim-1) + arr[dim-1]
	lw $ra, 4($sp) 				# ripristino $ra
	addi $sp, $sp, 8			# dealloco stack frame
	jr $ra						# ritorno alla procedura precedente
